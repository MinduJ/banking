//
//  NetworkError.swift
//  Banking
//
//  Created by MacbookPro on 27/6/18.
//  Copyright © 2018 Mincy. All rights reserved.
//

import Foundation

class NetworkError {
    
    var errorCode:Int?
    var errorDescription:String?
    
    init(errorCode:Int,errorDescription:String) {
        
        self.errorCode = errorCode
        self.errorDescription = errorDescription
        
    }
}
