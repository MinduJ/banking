//
//  Transaction.swift
//  Banking
//
//  Created by MacbookPro on 27/6/18.
//  Copyright © 2018 Mincy. All rights reserved.
//

import Foundation
/*
 
 {
 "id" : 1, "description" : "Opal recharge", "amount" : -65.80,  "effectiveDate" : "2017-12-21T08:40:51.620Z"
 }
 
 */

class Transaction: Codable {
    
    /// Overriding the property names, with custom property names
    /// when the json field is different, requires defining a `CodingKeys`
    /// enum and providing a case for each property. The case itself should
    /// match the property, and its rawValue of type string, should correspond
    /// to the JSON field name.
    enum CodingKeys: String, CodingKey {
        case transactionID = "id"
        case transactionDescription = "description"
        case transactionDate = "effectiveDate"
        case transactionAmount = "amount"
    }

    
    var transactionID:Int?
    var transactionDescription:String?
    var transactionDate:String?
    var transactionAmount:Double?
    
    init(transactionID: Int, transactionDescription: String, transactionDate: String,transactionAmount:Double) {
        self.transactionID = transactionID
        self.transactionDescription = transactionDescription
        self.transactionDate = transactionDate
        self.transactionAmount = transactionAmount
    }
    
    static func getCurrentAmount(transactions: [Transaction]) -> Double {
        
        var currentAmount: Double = 0
        
        for transaction in transactions {
            
            currentAmount += transaction.transactionAmount!
            
        }
        
        return currentAmount;
    }
    
   static func orderTransactionByDate(transactions:[Transaction]) -> [Transaction] {
        
        return transactions.sorted(by: {
            $0.transactionDate?.compare($1.transactionDate!) == .orderedDescending
        })
    }
    
    
}
