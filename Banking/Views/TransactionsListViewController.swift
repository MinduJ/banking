//
//  TransactionsListViewController.swift
//  Banking
//
//  Created by MacbookPro on 27/6/18.
//  Copyright © 2018 Mincy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TransactionsListViewController: UIViewController {

    @IBOutlet weak var transactionsTableView: UITableView!
    @IBOutlet weak var currentAmountLabel: UILabel!
    @IBOutlet weak var loadingIndicator:UIActivityIndicatorView!
    @IBOutlet weak var errorLabel: UILabel!
    
    private var disposeBag = DisposeBag()
     var transactionListViewModel:TransactionListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       // transactionListViewModel = TransactionListViewModel(transactionAPIManager: TransactionAPIManager(networking: NetworkingManager()))
        transactionListViewModel.fetchTransactions()

        setUpUI()

        bind()
        
        setupCellTapHandling()
        
    }
    
    private func setUpUI() {
        
        transactionsTableView.tableFooterView = UIView()
    }
    
    private func bind() {
    
        // Update the tableview when the transactions array is updated
        transactionListViewModel.transactions.asObservable()
            .bind(to: transactionsTableView.rx.items(cellIdentifier: TransactionTableViewCell.CellIdentifier, cellType: TransactionTableViewCell.self))                           {
                row, transaction, cell in
                cell.transaction = transaction
            }.disposed(by: disposeBag)
        
        // Update the current amount label when current amount is updated.
        transactionListViewModel.currentAmount.asObservable().subscribe(onNext: {[weak self] (currentAmount) in
            
            if(currentAmount > 0.0) {
                self?.currentAmountLabel.text = "Current Balance: $\(currentAmount)"
            }else {
                let transactionAmountString = String(currentAmount).replacingOccurrences(of: "-", with:"$")
                self?.currentAmountLabel.text = "Current Balance: -\(transactionAmountString)"
            }
        }).disposed(by: disposeBag)
        
        
        // Add observer for lodingIndicator view
        
        transactionListViewModel.isLoading.asDriver().drive(onNext: { [weak self] isLoading in
            
            if(isLoading == false) {
                 self?.loadingIndicator.stopAnimating()
            }
        }).disposed(by: disposeBag)
        
        // Add observer for error label
        
         transactionListViewModel.isError.asObservable().subscribe(onNext: {[weak self] isError in
            self?.errorLabel.isHidden = !isError
            self?.transactionsTableView.isHidden = isError
            self?.errorLabel.text = "Something went wrong!"
            
        }).disposed(by: disposeBag)
        
    }
    
    private func setupCellTapHandling() {
        
        transactionsTableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let newSelf = self else { return }
                
                //Fetch the selected transaction
                let transaction = newSelf.transactionListViewModel.transactions.value[indexPath.row]
                
                //Display an AlertView with details
                //Converting the date string to readable format
                let date = Utilities.getDateFromString(dateString: transaction.transactionDate!)
                let formattedDateString = Utilities.getStringFromDate(date: date!)
                let alertViewController:UIAlertController = Utilities.showAlertViewController(title: formattedDateString, message:"\(String(describing: transaction.transactionDescription!)) \(String(describing: transaction.transactionAmount!))")
                newSelf.present(alertViewController, animated: true, completion: nil)
                
                //Deselect the selected row
                if let selectedRowIndexPath = self?.transactionsTableView.indexPathForSelectedRow {
                    self?.transactionsTableView.deselectRow(at: selectedRowIndexPath, animated: true)
                }
            }) .disposed(by: disposeBag)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
