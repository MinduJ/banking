//
//  TransactionTableViewCell.swift
//  Banking
//
//  Created by MacbookPro on 27/6/18.
//  Copyright © 2018 Mincy. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    static var CellIdentifier:String = "TransactionTableViewCell"
    
    @IBOutlet weak var transactionDetailLabel: UILabel!
    @IBOutlet weak var transactionDateLabel: UILabel!
    @IBOutlet weak var transactionAmountLabel: UILabel!
    
    var transaction: Transaction? {
        didSet {
            guard let transaction = transaction else { return }
            transactionDetailLabel.text = transaction.transactionDescription
            
            //Converting the date string to readable format
            let date = Utilities.getDateFromString(dateString: transaction.transactionDate!)
            let formattedDateString = Utilities.getStringFromDate(date: date!)
            transactionDateLabel.text = formattedDateString
            
            if(transaction.transactionAmount! > 0.0) {
               transactionAmountLabel.text = "+$\(transaction.transactionAmount ?? 0)"
            }else {
                let transactionAmountString = String(transaction.transactionAmount!).replacingOccurrences(of: "-", with:"$")
                transactionAmountLabel.text = "-\(transactionAmountString)"
            }
        }
    }
        
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
