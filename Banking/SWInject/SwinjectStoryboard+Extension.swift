//
//  SwinjectStoryboard+Extension.swift
//  Banking
//
//  Created by MacbookPro on 28/6/18.
//  Copyright © 2018 Mincy. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard
import SwinjectAutoregistration

extension SwinjectStoryboard {
    @objc class func setup() {
        defaultContainer.autoregister(Networking.self, initializer: NetworkingManager.init)
        defaultContainer.autoregister(TransactionsFetcher.self, initializer: TransactionAPIManager.init)
        defaultContainer.autoregister(TransactionListViewModel.self, initializer: TransactionListViewModel.init)
        defaultContainer.storyboardInitCompleted(TransactionsListViewController.self) { resolver, controller in
            controller.transactionListViewModel = resolver ~> TransactionListViewModel.self
        }
    }
}
