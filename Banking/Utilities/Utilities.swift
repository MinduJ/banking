//
//  Utilities.swift
//  Banking
//
//  Created by MacbookPro on 27/6/18.
//  Copyright © 2018 Mincy. All rights reserved.
//

import UIKit
import Foundation

final class Utilities {
    
    
    static func showAlertViewController(title:String,message:String) -> UIAlertController {
        
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        alertViewController.addAction(cancelAction)
        return alertViewController
    }
    
    static func getDateFromString(dateString:String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX

        guard let date = dateFormatter.date(from: dateString) else { return nil }
        
        return date
        
    }
    
    static func getStringFromDate(date:Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, MMM d, yyyy - h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date)
        
        return timeStamp
    }
    
    
}
