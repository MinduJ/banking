//
//  TransactionsAPIManager.swift
//  Banking
//
//  Created by MacbookPro on 27/6/18.
//  Copyright © 2018 Mincy. All rights reserved.
//

import Foundation

protocol TransactionsFetcher {
    
    //func getTransactionsList(from URLString:String,completionHandler:@escaping ([Transaction]?,Swift.Error?) -> Void)
    func getTransactionsList(completionHandler:@escaping ([Transaction]?,Swift.Error?) -> Void)
}

class TransactionAPIManager: TransactionsFetcher {
    
    private let urlString = "https://private-ddc1b2-transactions14.apiary-mock.com/transactions"
    var networking:Networking
      
    init(networking: Networking) {
        self.networking = networking
    }
    
    func getTransactionsList(completionHandler:@escaping ([Transaction]?,Swift.Error?) -> Void)  {
        
        networking.URLToFetchRequest(urlString:urlString) { (response, error) in
            
            if let error = error {
                print("Error in fetching Transactions: \(error.localizedDescription)")
                completionHandler(nil,error)
            }
            
            // Parse data into a model object.
            let decoded = self.decodeJSON(type: [Transaction].self, from: response)
            if let decoded = decoded {
                //print("Transaction returned: \(String(describing: decoded.transactionDescription))")
                 completionHandler(decoded,nil)
            }
           
        }
    }
    
    private func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
        let decoder = JSONDecoder()
        guard let data = from,
            let response = try? decoder.decode(type.self, from: data) else { return nil }
        
        return response
    }
}
