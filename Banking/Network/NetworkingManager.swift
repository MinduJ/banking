//
//  NetworkingManager.swift
//  Banking
//
//  Created by MacbookPro on 27/6/18.
//  Copyright © 2018 Mincy. All rights reserved.
//

import Foundation
import Alamofire

protocol Networking {
    
    typealias CompletionHandler = (Data?, Swift.Error?) -> Void
    
    func URLToFetchRequest(urlString:String,completion: @escaping CompletionHandler)
}

class NetworkingManager:Networking {
    
    typealias CompletionHandler = (Data?, Swift.Error?) -> Void
    
     func URLToFetchRequest(urlString:String,completion: @escaping CompletionHandler) {
        
        guard  let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) else { return  }
        createRequest(from: url) { (data, error) in
            completion(data,error)
        }
        
    }
    
    private func createRequest(from url: URL,completion: @escaping CompletionHandler) {
        
        Alamofire.request(url)
            .validate()
            .response { (response) in
                
            completion(response.data,response.error)
                    
        }
    }
    
}
