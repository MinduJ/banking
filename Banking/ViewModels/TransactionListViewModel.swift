//
//  TransactionListViewModel.swift
//  Banking
//
//  Created by MacbookPro on 27/6/18.
//  Copyright © 2018 Mincy. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class TransactionListViewModel {
    
    let transactions = Variable<[Transaction]>([])
    let currentAmount: Variable<Double> = Variable(0)
    var isLoading: Variable<Bool> = Variable(true)
    var isError: Variable<Bool> = Variable(false)
    var error = Variable<Error?>(nil)
    
    let transactionsFetcher:TransactionsFetcher?
    
    init(transactionsFetcher: TransactionsFetcher) {
        self.transactionsFetcher = transactionsFetcher
    }
    
    func fetchTransactions() {
        
        guard let transactionsFetcher = transactionsFetcher else { fatalError("Missing dependencies") }
        transactionsFetcher.getTransactionsList (){ [weak self] (transactions, error) in
            
            guard let newSelf = self else { return }
            
            newSelf.isLoading.value = false
            
            if(transactions != nil) {
                newSelf.handleResponse(transactions: transactions!)
            }else {
                
                newSelf.handleError(error: error!)
            }
        }
    }
    
    
    private func handleResponse(transactions:[Transaction]) {
        
        isError.value = false
        
        self.transactions.value = transactions
        
        //Calculate the current amount
        self.currentAmount.value = Transaction.getCurrentAmount(transactions: self.transactions.value)
        
        // Sort the transactions array based on date
        self.transactions.value = Transaction.orderTransactionByDate(transactions: self.transactions.value)
    }
    
    
    
    private func handleError(error:Error) {
        
        isError.value = true
        self.error.value = error 
        
    }
    
    
}
