//
//  BankingTests.swift
//  BankingTests
//
//  Created by MacbookPro on 27/6/18.
//  Copyright © 2018 Mincy. All rights reserved.
//

import XCTest
import Swinject
import SwinjectAutoregistration

@testable import Banking

class BankingTests: XCTestCase {
    
    private let container = Container()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // Registrations for the network using Alamofire.
        container.register(Networking.self) { _ in NetworkingManager() }
        
        container.register(TransactionsFetcher.self) { resolver in
            TransactionAPIManager(networking: resolver.resolve(Networking.self)!)
        }
        
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        container.removeAll()
        super.tearDown()
    }
    
     func createData() -> [Transaction] {
        let transaction1 = Transaction(transactionID: 1, transactionDescription: "Opal recharge", transactionDate: "2017-12-21T08:40:51.620Z", transactionAmount: -65.80)
        let transaction2 = Transaction(transactionID: 2, transactionDescription: "Restaurant Pub Bar", transactionDate: "2018-01-19T09:20:21.150Z", transactionAmount: -20.50)
        let transaction3 = Transaction(transactionID: 3, transactionDescription: "Hotel Booking refund", transactionDate: "2018-01-05T11:30:11.370Z", transactionAmount: 1240.99)
        let transaction4 = Transaction(transactionID: 4, transactionDescription: "Air ticket", transactionDate: "2018-01-10T07:50:31.510Z", transactionAmount:-190.34)
        let transaction5 = Transaction(transactionID: 5, transactionDescription: "Transfer to account 1234", transactionDate: "2018-01-20T08:10:21.450Z", transactionAmount: -389.20)
        return [transaction1, transaction2, transaction3,transaction4,transaction5]
    }
    
    func testTransactionsAPI() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let fetcher = container.resolve(TransactionsFetcher.self)!
        let expectation = XCTestExpectation(description: "Fetch transactions list data")
        fetcher.getTransactionsList { (transactions,error ) in
            XCTAssertNotNil(transactions)
            XCTAssert((transactions?.count)! > 0, "Transactions data is recieved")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
        

    }
    
    func testOrderByDate() {
        
        let transactions = createData()
        
        let orderedByDateTransactions = Transaction.orderTransactionByDate(transactions:transactions)
        
        let expectedTransactionDescription = "Transfer to account 1234"
        
        let actualTransactionDescription = orderedByDateTransactions[0].transactionDescription
        
        XCTAssertEqual(expectedTransactionDescription, actualTransactionDescription)
    }
    
    func testGetCurrentAmount() {
        
        let transactions = createData()
        
        let actualCurrentAmount = NSString(format: "%.3f", Transaction.getCurrentAmount(transactions:transactions)) 
        
        let expectedCurrentAmount = NSString(format: "%.3f", 575.150)
        
        XCTAssertEqual(expectedCurrentAmount, actualCurrentAmount)
    }
    
    
    
    

    
}
