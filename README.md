# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Parsed a json feed.
Displays the current account balance based on transactions amount.
Displays the content in a list, ordered by effective Date.

* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

To get started with the project install Cocoapods using terminal. 
Refer https://cocoapods.org.
The below list of dependencies is in a text file named Podfile.
You can install the dependencies in project using the command: pod install.
Make sure to always open the Xcode workspace instead of the project file when building your project.

* Almofire
* RxSwift
* RxCocoa
* Swinject
* SwinjectStoryboard
* SwinjectAutoregistration 


### Requirements

* Cocopods
* Xcode 9.3 compatible

### Architecture Concepts

* MVVM
* Binding(using RxSwift)
* Dependency Injection (using Swinject)
* Dependencies management (using Cocoapods)
